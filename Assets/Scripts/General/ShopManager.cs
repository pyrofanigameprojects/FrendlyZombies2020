﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 0414 //unused variables
public class ShopManager : MonoBehaviour {

    //General Upgrades, basic attack damage and wall hp
    private readonly int[] basicDamageCosts = { 50, 70, 100 };
    private readonly int[] wallHealthCosts = { 35, 56, 70, 100 };

    //Ability 1-3 Upgrades || 2D array reprisenting the 3 upgradable abilities and the coosts of each one
    private readonly int[,] abilityUpgradeCost = { { 50, 80, 120 }, { 60, 85, 130 }, { 60, 80, 120 } };

    private GameController gameController;
    private AbilityController abilityController;
    private BazookaBulletBehavior bazookaBullet;
    private WallHealthBehaviour wallHealth;
    private PenetBulletBehavior pentratingBullet;
    private FireBullets fireBullets;

    public Image basicDamageBarSlider;
    public Image wallHpBarSlider;
    public Image brainsImage;
    public Button abilityUpgradeButton;
    public Text abilityCostText;
    public Text abilityDescriptionText;
    public Text brainsGatheredText;
    public Text basicDamageCostText, wallHpCostText;

    //Be extra carefull if you want to change the order of the items in the enum, since the ordering is used in code
    public enum AbilityType {PenetratingBullet = 1, MiniGunBullet = 2, BazookaBullet = 3, Default = 4};
    private AbilityType selectedAbility = AbilityType.Default;

    private void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        abilityController = FindObjectOfType<AbilityController>();
        bazookaBullet = FindObjectOfType<BazookaBulletBehavior>();
        wallHealth = FindObjectOfType<WallHealthBehaviour>();
        pentratingBullet = FindObjectOfType<PenetBulletBehavior>();
        fireBullets = FindObjectOfType<FireBullets>();
    }

    private void Start()
    {
        UpdateGatheredBrainsText();
        UpdateBasicDamageCostText();
        UpdateWallHpCostText();
        basicDamageBarSlider.fillAmount = CalculateFillAmount(PlayerPrefsManager.Instance.GetBasicDmgLevel() - 1, basicDamageCosts.Length); 
        wallHpBarSlider.fillAmount = CalculateFillAmount(PlayerPrefsManager.Instance.GetWallHpLevel() - 1, wallHealthCosts.Length);
    }

    public void AbilitySelectionButtonPressed(int abilityID)
    {
        if (selectedAbility == AbilityType.Default)
        {
            abilityCostText.gameObject.SetActive(true);
            brainsImage.gameObject.SetActive(true);
            abilityUpgradeButton.gameObject.SetActive(true);
            abilityDescriptionText.gameObject.SetActive(true);
        }

        selectedAbility = (AbilityType)abilityID;

        UpdateAbilityCostText();
        UpdateAbilityDescriptionText();
    }

    public void UpgradeBasicDamage()
    {
        int basicDmgLevel = PlayerPrefsManager.Instance.GetBasicDmgLevel();

        if(basicDmgLevel == basicDamageCosts.Length) 
        {
            print("Maximum level reached!");
        }
        else
        {
            int brainsAmount = PlayerPrefsManager.Instance.GetBrains();
            if(brainsAmount >= basicDamageCosts[basicDmgLevel])
            {
                PlayerPrefsManager.Instance.ModifyBrains( - basicDamageCosts[basicDmgLevel]);
                PlayerPrefsManager.Instance.IncrementBasicDmgLevel();

                float sliderfillAmount = CalculateFillAmount(basicDmgLevel, basicDamageCosts.Length);
                basicDamageBarSlider.fillAmount = sliderfillAmount;
                UpdateGatheredBrainsText();
                UpdateBasicDamageCostText();

                print("Basic damage upgraded! New level: " + PlayerPrefsManager.Instance.GetBasicDmgLevel());
            }
            else
            {
                print("Not Enough Brains!");
            }
        }
    }

    public void UpgradeWallHp()
    {
        int wallHpLevel = PlayerPrefsManager.Instance.GetWallHpLevel();

        if (wallHpLevel == wallHealthCosts.Length)
        {
            print("Maximum level reached!");
        }
        else
        {
            int brainsAmount = PlayerPrefsManager.Instance.GetBrains();
            if (brainsAmount >= wallHealthCosts[wallHpLevel])
            {
                PlayerPrefsManager.Instance.ModifyBrains( - wallHealthCosts[wallHpLevel]);
                PlayerPrefsManager.Instance.IncrementWallHpLevel();

                float sliderfillAmount = CalculateFillAmount(wallHpLevel, wallHealthCosts.Length);
                wallHpBarSlider.fillAmount = sliderfillAmount;
                UpdateGatheredBrainsText();
                UpdateWallHpCostText();

                print("Wall health upgraded! New level: " + PlayerPrefsManager.Instance.GetWallHpLevel());
            }
            else
            {
                print("Not Enough Brains!");
            }
        }
    }

    public void AbilityUpgradeButtonPressed()
    {
        if (selectedAbility == AbilityType.PenetratingBullet)
        {
            int penetratingBulletLevel = PlayerPrefsManager.Instance.GetPenetratingBulletLevel();

            if (penetratingBulletLevel < abilityUpgradeCost.GetLength(1))
            {
                UpgradeAbility(penetratingBulletLevel);
            }
        }
        else if (selectedAbility == AbilityType.MiniGunBullet)
        {
            int minigunBulletLevel = PlayerPrefsManager.Instance.GetMiniGunBulletLevel();

            if (minigunBulletLevel < abilityUpgradeCost.GetLength(1))
            {
                UpgradeAbility(minigunBulletLevel);
            }
        }
        else
        {
            int bazookaBulletlevel = PlayerPrefsManager.Instance.GetBazookaBulletLevel();

            if (bazookaBulletlevel < abilityUpgradeCost.GetLength(1))
            {
                UpgradeAbility(bazookaBulletlevel);
            }
        }
    }

    private void UpgradeAbility(int abilityLevel)
    {
        int brainsAmount = PlayerPrefsManager.Instance.GetBrains();
        int costToUpgrade = abilityUpgradeCost[(int)selectedAbility - 1, abilityLevel];

        if (brainsAmount >= costToUpgrade) 
        {
            PlayerPrefsManager.Instance.ModifyBrains(-costToUpgrade);

            if(selectedAbility == AbilityType.PenetratingBullet) //is this if worth the standalone method?
            {
                PlayerPrefsManager.Instance.IncrementPenetratingBulletLevel();
            }else if(selectedAbility == AbilityType.MiniGunBullet)
            {
                PlayerPrefsManager.Instance.IncrementMinigunBulletLevel();
            }
            else
            {
                PlayerPrefsManager.Instance.IncrementBazookaBulletLevel();
            }
            
            UpdateAbilityCostText();
            UpdateGatheredBrainsText();
            print(selectedAbility + " upgraded!");
        }
        else
        {
            print("Not Enough Brains!"); //just play a sound?
        }
    }

    private void UpdateAbilityCostText()
    {
        if (selectedAbility == AbilityType.PenetratingBullet)
        {
            ChangeText(PlayerPrefsManager.Instance.GetPenetratingBulletLevel());
        }
        else if (selectedAbility == AbilityType.MiniGunBullet)
        {
            ChangeText(PlayerPrefsManager.Instance.GetMiniGunBulletLevel());
        }
        else
        {
            ChangeText(PlayerPrefsManager.Instance.GetBazookaBulletLevel());
        }
    }

    private void ChangeText(int abilityLevel)
    {
        if (abilityLevel >= abilityUpgradeCost.GetLength(1))
        {
            abilityCostText.text = "Max!";
        }
        else
        {
            abilityCostText.text = abilityUpgradeCost[(int)selectedAbility - 1, abilityLevel] + "";
        }
    }

    private void UpdateAbilityDescriptionText()
    {
        abilityDescriptionText.text = "Damage upgrade for ability " + (int)selectedAbility;
    }

    private float CalculateFillAmount(int currentLevel, int maximumLevel)
    {
        return (float)(1 * (currentLevel + 1)) / maximumLevel;
    }

    private void UpdateGatheredBrainsText()
    {
        brainsGatheredText.text = PlayerPrefsManager.Instance.GetBrains() + "";
    }

    private void UpdateBasicDamageCostText()
    {
        int basicDamageLevel = PlayerPrefsManager.Instance.GetBasicDmgLevel();
        if (basicDamageLevel == basicDamageCosts.Length)
        {
            basicDamageCostText.text = "Max!";
        }
        else
        {
            basicDamageCostText.text = basicDamageCosts[basicDamageLevel] + "";
        }
    }

    private void UpdateWallHpCostText()
    {
        int wallHpLevel = PlayerPrefsManager.Instance.GetWallHpLevel();
        if (wallHpLevel == wallHealthCosts.Length)
        {
            wallHpCostText.text = "Max!";
        }
        else
        {
            wallHpCostText.text = wallHealthCosts[wallHpLevel] + "";
        }
    }

}
