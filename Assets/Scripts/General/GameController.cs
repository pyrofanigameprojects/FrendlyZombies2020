﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : UnitySingleton<GameController>
{
    public Text brainsCollectedText;
    public static float height;
    public static float width;
	public GameObject DeathPanel;
    int brainCounterInGame = 0;
    bool gameOver;
    Camera cam;

    override protected void SingletonAwake()
    {
        UnitySingleton<GameController>.Instance.CameraSetup();
        FindObjectOfType<WallHealthBehaviour>().wallDownEvent += UnitySingleton<GameController>.Instance.OnWallDown;
    }

    void Start()
    {
        UpdateBrainsText();
        CameraSetup();
        gameOver = false;
    }

    public void GiveBrainsInGame()
    {
        brainCounterInGame++;
        UpdateBrainsText();
    }
    void UpdateBrainsText()
    {
        brainsCollectedText.text = brainCounterInGame.ToString();
    }

    public bool GameOver()
    {
        return gameOver;
    }

   public void OnWallDown()
    {
        gameOver = true;
        DeathPanel.SetActive(true);
        Time.timeScale = 0;
    }

  public void CameraSetup()
    {
        cam = Camera.main;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
    }
}   

   