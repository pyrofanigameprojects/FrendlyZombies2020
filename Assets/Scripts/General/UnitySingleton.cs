﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour{
   
    public static T Instance { get; private set; }
    // Do not override
    protected void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = GetComponent<T>();
            SingletonAwake();
        }
    }

    protected virtual void SingletonAwake()
    {

        
        // Override this method if you need Awake
        // because in Awake() object might be destroyed before it gets to the overridden Awake
    }
    
}
