﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UiMenuManager : MonoBehaviour {

   public void Resume() //it changes the timescale which is usually used in slow time mechanism in order to resume the game. 1 means 100% speed
    {
        Time.timeScale = 1;
    }
    public void Pause()
    {
        Time.timeScale = 0;
    }
    public void LoadA(string Scene)// it loads a scene with the given name
    {
        SceneManager.LoadScene(Scene);
    }
    public void ExitQuitGame()//it closes the app
    {
        Application.Quit();
    }
}
