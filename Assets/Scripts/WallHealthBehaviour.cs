﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallHealthBehaviour : MonoBehaviour {
    [SerializeField]
    public static float maxHealth = 100f;
    public readonly int[] healthUpgrades = { 0, 20, 45, 80, 120 };
    public delegate void WallDown();
    public WallDown wallDownEvent;
    public static WallHealthBehaviour instance;
    Image healthBar;
    float currentHealthValue;
    
    void Start ()
    {
        healthBar = GetComponent<Image>();
        maxHealth += healthUpgrades[PlayerPrefsManager.Instance.GetWallHpLevel()];
        currentHealthValue = maxHealth;
    }

    void Update ()
    {
        healthBar.fillAmount = currentHealthValue / maxHealth;
	}

    public void TakeDamage(float damage)
    {
        currentHealthValue -= damage;
        if (currentHealthValue <= 0)
        {
            wallDownEvent?.Invoke();
        }
      
    }

    public void UpgradeHealth(int boost)
    {
        maxHealth += boost;
    }

}
