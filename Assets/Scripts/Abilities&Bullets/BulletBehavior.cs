﻿/* @Petros
 * script attached to Bullet prefab
 * attach the player to the fireBullet reference
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    public event Action<BulletBehavior> OnbulletDestroyed;
    private int bulletSpeed = 15;
    public float damage; //obtained from FireBullets script depending on whether the mingun ability is active


    void Update()
    {
        transform.Translate(0, bulletSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            col.GetComponent<EnemyCore>().TakeDamage(damage);
        }
        DestroyBullet();
    }

    private void DestroyBullet()
    {
        OnbulletDestroyed(this);  //when disabled,notify the subscribed method EnqueueBullet() of FireBullets script to enqeue this bullet
        gameObject.SetActive(false);       
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

}
