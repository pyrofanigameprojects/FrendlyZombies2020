﻿/* @Petros
 * script attached to a game manager empty object
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityController : MonoBehaviour
{
    public event Action<bool> togglePlayerFire; //takes as parameter (true) to make the player fire, or (false) to make him stop firing
    public event Action<bool> OnMinigunActivated;

    private float[] abilityCooldowns = { 15f, 25f, 20f, 12f };
    [SerializeField]
    private float[] elapsedAbilityCooldowns = { 0f, 0f, 0f, 0f }; //remaining seconds before cooldown is reset

    //ability 2
    private bool ability2IsActive = false;
    private const float ability2Duration = 4f;
    private const float fireRateBoost = 0.7f;

    //ability 4
    private bool ability4IsActive = false;
    private const float ability4Duration = 6f;
    private const float speedBoost = 2f;


    private FireBullets fireBulletsScript;
    private PlayerMovement playerMovementScript;


    private void Awake()
    {
        fireBulletsScript = FindObjectOfType<FireBullets>();
        playerMovementScript = FindObjectOfType<PlayerMovement>();
    }

    private void Update()
    {
        if (elapsedAbilityCooldowns[3] != 0)
        {
            elapsedAbilityCooldowns[3] -= Time.deltaTime;
            if (elapsedAbilityCooldowns[3] < 0) elapsedAbilityCooldowns[3] = 0;
        }

        if (elapsedAbilityCooldowns[2] != 0)
        {
            elapsedAbilityCooldowns[2] -= Time.deltaTime;
            if (elapsedAbilityCooldowns[2] < 0) elapsedAbilityCooldowns[2] = 0;
        }

        if (elapsedAbilityCooldowns[1] != 0)
        {
            elapsedAbilityCooldowns[1] -= Time.deltaTime;
            if (elapsedAbilityCooldowns[1] < 0) elapsedAbilityCooldowns[1] = 0;
        }

        if (elapsedAbilityCooldowns[0] != 0)
        {
            elapsedAbilityCooldowns[0] -= Time.deltaTime;
            if (elapsedAbilityCooldowns[0] < 0) elapsedAbilityCooldowns[0] = 0;
        }
    }


    public void TriggerAbilityPenetratingBullet()
    {
        if (elapsedAbilityCooldowns[0] == 0)
        {
            if (ability2IsActive) ResetAbility2();
            togglePlayerFire(false);
            fireBulletsScript.FirePenetratingBullet();
            elapsedAbilityCooldowns[0] = abilityCooldowns[0];
        }
    }

    public void TriggerAbilityMiniGun()
    {
        if (!ability2IsActive && elapsedAbilityCooldowns[1] == 0)
        {
            ability2IsActive = true;
            OnMinigunActivated(true);  //FireBullets script
            Invoke(nameof(ResetAbility2), ability2Duration);
        }
    }

    public void TriggerAbilityBazooka()
    {
        if (elapsedAbilityCooldowns[2] == 0)
        {
            if (ability2IsActive) ResetAbility2();
            togglePlayerFire(false);
            fireBulletsScript.FireBazookaBullet();
            elapsedAbilityCooldowns[2] = abilityCooldowns[2];
        }
    }

    public void TriggerSupportAbility()
    {
        if (!ability4IsActive && elapsedAbilityCooldowns[3] == 0)
        {
            ability4IsActive = true;
            playerMovementScript.SetSuperSpeedMode(true);
            Invoke(nameof(ResetAbility4), ability4Duration);
        }
    }

    private void ResetAbility2()
    {
        CancelInvoke(nameof(ResetAbility2)); //strange bug without canceling, probably conflict while trying to call ResetAbility2 before it was invoked normaly
        ability2IsActive = false;
        OnMinigunActivated(false);
        elapsedAbilityCooldowns[1] = abilityCooldowns[1];
    }

    private void ResetAbility4()
    {
        ability4IsActive = false;
        playerMovementScript.SetSuperSpeedMode(false);
        elapsedAbilityCooldowns[3] = abilityCooldowns[3];
    }

}
