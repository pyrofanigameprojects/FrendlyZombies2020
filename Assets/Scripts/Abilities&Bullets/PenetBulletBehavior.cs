﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenetBulletBehavior : MonoBehaviour
{
    private int bulletSpeed = 12;
    private FireBullets fireB;
    private float damageBoost;
    public readonly float[] damageUpgrades = { 0, 6, 8, 12 };
    public static float damage = 50;  


    private void Start()
    {
        fireB = FindObjectOfType<FireBullets>();
        damageBoost = damageUpgrades[PlayerPrefsManager.Instance.GetPenetratingBulletLevel()];
    }

    void Update ()
    {
        transform.Translate(0, bulletSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            print("Penetrating bullet hit!");
            col.GetComponent<EnemyCore>().TakeDamage(damage + damageBoost);
        }

        if (col.CompareTag("BulletDestruction"))
        {
            fireB.canFire = true;
            Destroy(gameObject);
        } 
    }

}
