﻿/* @Petros
 * script attached to Player prefab
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullets : MonoBehaviour
{
    public BulletBehavior bullet;
    public BazookaBulletBehavior bazookaBullet;
    public PenetBulletBehavior penetBullet;
    public Sprite[] bulletSprites;
    [HideInInspector]
    public bool canFire = true;

    private Queue<BulletBehavior> bulletQueue;
    private readonly int bulletsAmount = 15;
    private float shootBulletTime = 0.8f; //the lower, the faster the player will shoot bullets 
    private int currentBulletSprite = 0;
    private float minigunBulletDamage = 4.5f;
    private float normalBulletDamage = 10;
    private float currentBulletDamage;
    private float normalBulletDamageBoost;
    private float minigunBulletDamageBoost;
    public readonly float[] normalBulletDmgUpgrades = { 0, 1.3f, 3f, 5.2f };
    public readonly float[] miniGunBulletDmgUpgrades = { 0, 0.5f, 1.2f, 2.1f };

    private AbilityController abilityControllerScript;


    private void Awake()
    {
        abilityControllerScript = FindObjectOfType<AbilityController>();
    }

    void Start()
    {
        abilityControllerScript.togglePlayerFire += PlayerCanFire;  //subscribing the method PlayerCanFire to the delegate.
        abilityControllerScript.OnMinigunActivated += SetMinigunMode;

        currentBulletDamage = normalBulletDamage;

        bulletQueue = new Queue<BulletBehavior>();
        for (int i = 0; i <= bulletsAmount; i++)  //initializing Queue with "bulletsAmount" number of bullet prefabs
        {
            BulletBehavior Bul = Instantiate(bullet);
            bulletQueue.Enqueue(Bul);
            Bul.OnbulletDestroyed += EnqueueBullet;
            Bul.gameObject.SetActive(false);
        }
        Invoke("Fire", shootBulletTime);

        setDamageBoosts();
    }

    private void setDamageBoosts()
    {
        normalBulletDamageBoost = normalBulletDmgUpgrades[PlayerPrefsManager.Instance.GetBasicDmgLevel()];
        minigunBulletDamageBoost = miniGunBulletDmgUpgrades[PlayerPrefsManager.Instance.GetMiniGunBulletLevel()];
    }

    private void EnqueueBullet(BulletBehavior bullet) //puts the disabled bullet that has been fired inside the queue again
    {
        bulletQueue.Enqueue(bullet);
    }

    private void PlayerCanFire(bool toggleFire)
    {
        canFire = toggleFire;
    }

    private void Fire()
    {
        if (canFire)
        {
            var Bul = bulletQueue.Dequeue(); //removing bullet from queue when fired
            Bul.transform.position = new Vector3(transform.position.x, transform.position.y, 0); //aligning the bullet with the position of the player
            Bul.GetComponent<SpriteRenderer>().sprite = bulletSprites[currentBulletSprite];      //setting the appropriate sprite to the bullet

            float damageBoost;
            if (currentBulletDamage == normalBulletDamage)
            {
                damageBoost = normalBulletDamageBoost;
            }
            else
            {
                damageBoost = minigunBulletDamageBoost;
            }
            Bul.GetComponent<BulletBehavior>().damage = currentBulletDamage + damageBoost;       //depending on whether it is a normal bullet or minigun bullet

            Bul.gameObject.SetActive(true);
        }
        Invoke("Fire", shootBulletTime); 
    }

    public void FireBazookaBullet()
    {
        Instantiate(bazookaBullet.gameObject, transform.position, Quaternion.identity);
    }

    public void FirePenetratingBullet()
    {
        Instantiate(penetBullet.gameObject, transform.position, Quaternion.identity);
    }

    public void SetMinigunMode(bool minigunEnabled)
    {
        if (minigunEnabled)
        {
            currentBulletSprite = 1;  //setting the bullet sprite to the minigun bullet
            shootBulletTime = 0.1f;
            currentBulletDamage = minigunBulletDamage;
        }
        else
        {
            currentBulletSprite = 0;  //resetting the normal bullet sprite
            shootBulletTime = 0.8f;
            currentBulletDamage = normalBulletDamage;
        }
    }

}
