﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class EnemyCore : MonoBehaviour {
    public event Action<EnemyCore> OnEnemyKilled;
    public event Action<float> OnWallHit;               
    private string category;
    private float speed;
    private float damage;
    private float currentHealth, healthResetValue;
    private float attackSpeed;
    private bool canAttack = false;
    private float attackTimer;
    private float wallPosition;
    [SerializeField]
    private Image healthBar;


    protected void Init(string Category, float Speed, float Damage, float AttackSpeed ,float Health)
    {
        category = Category;
        speed = Speed;
        damage = Damage;
        attackSpeed = AttackSpeed;
        currentHealth = healthResetValue = Health;
        healthBar.fillAmount = 1;
        wallPosition = -GameController.height / 7;
    }

 

    protected void Move()
    {
        if (GameController.Instance.GameOver() == false)
        {
            if (gameObject.transform.position.y > wallPosition)
            {
                gameObject.transform.Translate(0, -speed * Time.deltaTime, 0);
            }
            else
            {
                canAttack = true;
            }
        }
    }

    protected void Attack()
    {
        if (GameController.Instance.GameOver() == false && canAttack == true)
        { 
            attackTimer += Time.deltaTime;
            if (attackTimer >= attackSpeed)
            {
                attackTimer = 0;
                OnWallHit(damage);  
            }

        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;

        healthBar.fillAmount =  currentHealth / healthResetValue;
        if (currentHealth <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        OnEnemyKilled(this);
    }

    public void ResetEnemy()
    {
        canAttack = false; 
        currentHealth = healthResetValue;
        healthBar.fillAmount = 1;
    }
}


