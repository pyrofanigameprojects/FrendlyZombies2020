﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPooling : UnitySingleton<EnemyPooling>
{
    [SerializeField]
    EnemyCore enemyPrefab;
    [SerializeField]
    float fixedYSpawnPosition;
    [SerializeField]
    const int NumberOfLanes = 4;
    [SerializeField]
    int enemiesNeededForLevelUp;
    [SerializeField]
    float difficultyMultiplier = 1 / 8;

    Vector2 poolSpawnPosition = new Vector2(-10f, -10f);
    int enemyPoolSize = 15;
    float gameDifficulty = 1;
    float spawnTimer = 4f;
    float spawnProbability = 0.4f;
    int enemiesKilledCounter = 0;
    float[] laneSpawnPointXCoordinates;
    float[] laneSpawnPosition;
    Queue<EnemyCore> enemyPool;

    void Start()
    {
        InitializeLanes();
        InitializeEnemies();
        InvokeRepeating("SpawnEnemy", 0, spawnTimer / gameDifficulty);
    }

    void InitializeLanes()
    {
        laneSpawnPosition = new float[NumberOfLanes];
        float[] tempLaneSpaces = new float[NumberOfLanes + 1];
        laneSpawnPointXCoordinates = new float[NumberOfLanes];

        float laneX = GameController.width / 2;

        float increment = GameController.width / NumberOfLanes;
        for (int i = 0; i < NumberOfLanes + 1; i++)
        {
            tempLaneSpaces[i] = laneX;
            laneX -= increment;
        }

        for (int i = 0; i < NumberOfLanes; i++)
        {
            laneSpawnPointXCoordinates[i] = (tempLaneSpaces[i] + tempLaneSpaces[i + 1]) / 2;
        }
    }

    void InitializeEnemies()
    {
        enemyPool = new Queue<EnemyCore>(enemyPoolSize);
        for (int i = 0; i < enemyPoolSize; i++)
        {
            EnemyCore e = Instantiate(enemyPrefab, poolSpawnPosition, Quaternion.identity);
            e.gameObject.SetActive(false);
            e.OnEnemyKilled += EnqueueEnemy;
            e.OnWallHit += FindObjectOfType<WallHealthBehaviour>().TakeDamage;
            enemyPool.Enqueue(e);
        }
    }

    void SpawnEnemy()
    {
        print("Trying to spawn...");
        if (GameController.Instance.GameOver())
        {
            CancelInvoke();
        }
        else
        {
            RandomizeValues();
            for (int i = 0; i < NumberOfLanes; i++)
            {
                if (laneSpawnPosition[i] != 0)
                {
                    var enemy = enemyPool.Dequeue();
                    int temp = Random.Range(1, 3);
                    float offset = (temp == 1) ? offset = i : offset = -i;
                    if (offset < -2) offset += temp + 0.75f;
                    enemy.transform.position = new Vector3(laneSpawnPosition[i],fixedYSpawnPosition  + offset,0);
                    enemy.gameObject.SetActive(true);
                }
            }
        }
    }

    void RandomizeValues()
    {
        bool hasSpawnedAtLeastOneEnemy = false;
        for (int i = 0; i < NumberOfLanes; i++)
        {
            var x = Random.value;
            if (x <= spawnProbability * gameDifficulty)
            {
                hasSpawnedAtLeastOneEnemy = true;
                switch (i)
                {
                    case 1:
                        float offset = Random.Range(-0.1f, 0.2f);
                        laneSpawnPosition[i] = laneSpawnPointXCoordinates[i] + offset;
                        Debug.Log("OFFSET: "+ offset);
                        break;
                    case NumberOfLanes:
                        offset = Random.Range(-0.2f, 0.2f);
                        laneSpawnPosition[i] = laneSpawnPointXCoordinates[i] + offset;
                        Debug.Log("OFFSET: " + offset);
                        break;
                    default:
                        offset = Random.Range(-0.2f, 0.1f);
                        laneSpawnPosition[i] = laneSpawnPointXCoordinates[i] + offset;
                        Debug.Log("OFFSET: " + offset);
                        break;
                }
            }
            else
            {
                laneSpawnPosition[i] = 0;
            }
        }

        if (!hasSpawnedAtLeastOneEnemy) //if it failed to spawn an enemy in all lanes, then spawn one for sure in a random lane
        {
            var laneToSpawn = Random.Range(0, NumberOfLanes - 1);
            var offset = Random.Range(-0.2f, 0.2f);
            laneSpawnPosition[laneToSpawn] = laneSpawnPointXCoordinates[laneToSpawn] + offset;
        }

        if (enemiesKilledCounter >= enemiesNeededForLevelUp)
        {
            Debug.Log("Current difficulty: "+gameDifficulty);
            gameDifficulty  += gameDifficulty * difficultyMultiplier;
            Debug.Log("Upgraded difficulty: " + gameDifficulty);
            enemiesKilledCounter = 0;
        }
    }

    void EnqueueEnemy(EnemyCore enemy)
    {
        enemiesKilledCounter++;
        GameController.Instance.GiveBrainsInGame();
        enemy.ResetEnemy();
        enemy.gameObject.SetActive(false);
        enemyPool.Enqueue(enemy);
    }

    void OnDrawGizmos()
    {
        Camera cam = Camera.main;
        float height = 2f * cam.orthographicSize;
        float width = height * cam.aspect;

        float[] laneGizmosArray = new float[NumberOfLanes+1];
        float laneX = width / 2;

        float increment = width / NumberOfLanes;
        for (int i = 0; i < NumberOfLanes + 1; i++)
        {
            laneGizmosArray[i] = laneX;
            laneX -= increment;
        }

        float[] spawnPoints = new float[NumberOfLanes];
        for (int i = 0; i < NumberOfLanes; i++)
        {
            spawnPoints[i] = (laneGizmosArray[i] + laneGizmosArray[i + 1]) / 2;
        }

        Gizmos.color = Color.magenta;
        for (int i = 0; i < NumberOfLanes+1; i++)
        {
            Gizmos.DrawLine(new Vector3(laneGizmosArray[i], height/2.5f, 0), new Vector3(laneGizmosArray[i], -height / 2, 0));
        }

        Gizmos.color = Color.cyan;
        for (int i = 0; i < NumberOfLanes; i++)
        {
            Gizmos.DrawLine(new Vector3(spawnPoints[i], height / 2.5f, 0), new Vector3(spawnPoints[i], -height / 2, 0));
        }

        float wallPosition = -height / 7;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(-width/2, wallPosition, 0), new Vector3(width/2, wallPosition, 0));
    }
}
