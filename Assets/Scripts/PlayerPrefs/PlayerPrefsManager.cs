﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : UnitySingleton<PlayerPrefsManager>
{
    public const string BrainsPrefKey = "Brains";
    public const string BasicDmgLevelPrefKey = "NormalBulletDmg";
    public const string WallHpLevelPrefKey = "WallHp";
    public const string PenetratingBulletLevelPrefKey = "Ability1", MiniGunBulletLevelPrefKey = "Ability2", BazookaBulletLevelPrefKey = "Ability3";
    public const string HighScoreKey = "HighScoreV1"; 
    public const string MusicPrefKey = "Music";

    public const string SFXPrefKey = "SFX";

    public const int StateEnabledValue = 1;
    public const int StateDisabledValue = 0;

    private const int DefaultBrains = 0;
    private const int DefaultWallHpLevel = 0;
    private const int DefaultBasicDmgLevel = 0;
    private const int DefaultPenetratingBulletLevel = 0, DefaultMiniGunLevel = 0, DefaultBazookaBulletLevel = 0;


    public int GetHighScore()
    {
        return PlayerPrefs.GetInt(HighScoreKey, 0); // Default high score is always zero
    }

    public void ChangeHighScoreIfHigher(int sessionScore)
    {
        if (sessionScore > GetHighScore())
        {
            PlayerPrefs.SetInt(HighScoreKey, sessionScore);
        }
    }

    public int GetBrains()
    {
        return PlayerPrefs.GetInt(BrainsPrefKey, DefaultBrains);
    }
    public void ModifyBrains(int difference)
    {
        PlayerPrefs.SetInt(BrainsPrefKey, GetBrains() + difference);
    }

    public int GetBasicDmgLevel()
    {
        return PlayerPrefs.GetInt(BasicDmgLevelPrefKey, DefaultBasicDmgLevel);
    }
    public void IncrementBasicDmgLevel()
    {
        PlayerPrefs.SetInt(BasicDmgLevelPrefKey, GetBasicDmgLevel() + 1);
    }

    public int GetWallHpLevel()
    {
        return PlayerPrefs.GetInt(WallHpLevelPrefKey, DefaultWallHpLevel);
    }
    public void IncrementWallHpLevel()
    {
        PlayerPrefs.SetInt(WallHpLevelPrefKey, GetWallHpLevel() + 1);
    }

    public int GetPenetratingBulletLevel()
    {
        return PlayerPrefs.GetInt(PenetratingBulletLevelPrefKey, DefaultPenetratingBulletLevel);
    }
    public void IncrementPenetratingBulletLevel()
    {
        PlayerPrefs.SetInt(PenetratingBulletLevelPrefKey, GetPenetratingBulletLevel() + 1);
    }

    public int GetMiniGunBulletLevel()
    {
        return PlayerPrefs.GetInt(MiniGunBulletLevelPrefKey, DefaultMiniGunLevel);
    }
    public void IncrementMinigunBulletLevel()
    {
        PlayerPrefs.SetInt(MiniGunBulletLevelPrefKey, GetMiniGunBulletLevel() + 1);
    }

    public int GetBazookaBulletLevel()
    {
        return PlayerPrefs.GetInt(BazookaBulletLevelPrefKey, DefaultBazookaBulletLevel);
    }
    public void IncrementBazookaBulletLevel()
    {
        PlayerPrefs.SetInt(BazookaBulletLevelPrefKey, GetBazookaBulletLevel() + 1);
    }

    public void ResetUpgrades()
    {
        PlayerPrefs.SetInt(BasicDmgLevelPrefKey, 0);
        PlayerPrefs.SetInt(WallHpLevelPrefKey, 0);
        PlayerPrefs.SetInt(PenetratingBulletLevelPrefKey, 0);
        PlayerPrefs.SetInt(MiniGunBulletLevelPrefKey, 0);
        PlayerPrefs.SetInt(BazookaBulletLevelPrefKey, 0);
    }

    public void SetMusicState(bool state)
    {
        PlayerPrefs.SetInt(MusicPrefKey, state ? StateEnabledValue : StateDisabledValue);
    }

    public bool GetMusicState()
    {
        return PlayerPrefs.GetInt(MusicPrefKey, StateEnabledValue) == StateEnabledValue; // By default music is enabled
    }
}