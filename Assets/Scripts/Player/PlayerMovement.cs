﻿/* @Petros
 * script attached to Player prefab
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public GameObject leftBound;
    public GameObject rightBound;
    public GameObject CharacterMovingArea;

    private float playerSpeed = 3.6f;
    private int moveDirection;
    private bool isCharacterMoving = false;


    void Update()
    {
        //TODO: stop the movement after gameover
        // Just gets position of mouse Converts screen to world point position and if mouse pos.x > Character moving area, move character right else move character left 
        if (isCharacterMoving)
        {
            Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (mouse.x > CharacterMovingArea.transform.position.x)
                moveDirection = 1;
            else if (mouse.x < CharacterMovingArea.transform.position.x)
                moveDirection = -1;

            if ((transform.position.x < rightBound.GetComponent<Transform>().position.x || moveDirection == -1) && (transform.position.x > leftBound.GetComponent<Transform>().position.x || moveDirection == 1))
                transform.Translate(moveDirection * playerSpeed * Time.deltaTime, 0, 0);   //moves or doesn't move the player depending on the moveDirection         
        }
    }

    // onTriggerEnter Character is moving
    public void CharacterMoving()
    {
        isCharacterMoving = true;
    }

    // onTriggerExit Character stops moving
    public void CharacterNotMoving()
    {
        isCharacterMoving = false;
    }

    public void SetSuperSpeedMode(bool enableSuperSpeed)
    {
        if (enableSuperSpeed) playerSpeed = 5.6f;
        else playerSpeed = 3.6f;
    }

}
