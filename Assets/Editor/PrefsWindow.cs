﻿#if UNITY_EDITOR
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ZombieDefencePrefsWindow : EditorWindow
{
    [SerializeField]
    private int? brains;
    private int? wallHp;

    [MenuItem("Ninja Fusion/Player Prefs")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(ZombieDefencePrefsWindow));
    }

    private void OnGUI()
    {
        PlayerPrefsArea();
        //TutorialClearArea();
        Brains_Hp_AbilitiesArea();
    }

    private void PlayerPrefsArea()
    {
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Label("Player Preferences", EditorStyles.boldLabel);
        var defaultColor = GUI.backgroundColor;
        GUI.backgroundColor = Color.red * .7f;
        if (GUILayout.Button("Clear player prefs"))
        {
            UnityEngine.PlayerPrefs.DeleteAll();
            Debug.Log("Player prefs cleared!");

            brains = null;
            wallHp = null;
        }
        if (GUILayout.Button("Clear ability levels"))
        {
            if(PlayerPrefsManager.Instance != null)
            {
                PlayerPrefsManager.Instance.ResetUpgrades();
                Debug.Log("Ability levels cleared!");

                wallHp = null;
            }
            else
            {
                Debug.LogWarning("Error, could not clear upgrades. Try pressing this button while the game is running");
            }
        }
        GUI.backgroundColor = defaultColor;
        EditorGUILayout.EndVertical();
    }

    //private void TutorialClearArea()
    //{
    //    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
    //    GUILayout.Label("Tutorial", EditorStyles.boldLabel);
    //    var defaultColor = GUI.backgroundColor;
    //    GUI.backgroundColor = Color.yellow * .7f;
    //    if (GUILayout.Button("Clear tutorial visited flag"))
    //    {
    //        PlayerPrefs.SetInt(PlayerPrefsManager.TutorialHasBeenVisitedOncePrefKey, PlayerPrefsManager.SceneHasNotBeenVisited);
    //        Debug.Log("Tutorial visited flag cleared!");
    //    }
    //    GUI.backgroundColor = defaultColor;
    //    EditorGUILayout.EndVertical();
    //}

    private void Brains_Hp_AbilitiesArea()
    {
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("Brains value", EditorStyles.boldLabel);
        BrainsArea();
        GUILayout.Label("Wall Hp value", EditorStyles.boldLabel);
        WallHpArea();

        EditorGUILayout.EndVertical();
    }

    private void BrainsArea()
    {
        EditorGUILayout.BeginHorizontal();
        if (!brains.HasValue)
        {
            brains = PlayerPrefs.GetInt(PlayerPrefsManager.BrainsPrefKey, 0);
        }
        brains = EditorGUILayout.IntSlider("Brains", brains.Value, 0, 9999);
        if (GUILayout.Button("Set brains"))
        {
            PlayerPrefs.SetInt(PlayerPrefsManager.BrainsPrefKey, brains.Value);
            Debug.Log("Set brains : " + brains);
        }
        EditorGUILayout.EndHorizontal();
    }

    private void WallHpArea()
    {
        EditorGUILayout.BeginHorizontal();
        if (!wallHp.HasValue)
        {
            wallHp = PlayerPrefs.GetInt(PlayerPrefsManager.WallHpLevelPrefKey, 0);
        }
        wallHp = EditorGUILayout.IntSlider("Wall HP", wallHp.Value, 0, 4);
        if (GUILayout.Button("Set Wall Hp"))
        {
            PlayerPrefs.SetInt(PlayerPrefsManager.WallHpLevelPrefKey, wallHp.Value);
            Debug.Log("Set Wall Hp : " + wallHp);
        }
        EditorGUILayout.EndHorizontal();
    }
}
#endif